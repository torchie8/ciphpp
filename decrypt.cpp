// This part of the program takes care of converting ciphers back to text.

#include "nonportable.h"
#include "global.h"

class bad_password_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: incorrect password";
	}
};

class bad_format_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: invalid cipher format";
	}
};

class old_version_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: source is newer version (try updating)";
	}
};

class bad_cipher_len_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: invalid cipher length";
	}
};

class file_read_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: error reading source file";
	}
};

extern char DEFAULT_CHAR_TABLE [DEFAULT_TABLE_SIZE];

extern char *char_table;

int incr_r;

// Reads one block (2 numbers) from string cipher at a given index pos and converts to int
int getBlock(std::string cipher, int c_index)
{	
	int block_code = (int)(cipher[c_index] - '0') * 10; // convert char 1 to int
	c_index++;                                          // move to char 2
	block_code += (int)(cipher[c_index] - '0');         // convert char 2 to int
	if (c_index > 7) {
		block_code -= incr_r;                       // decrement by incr_r (skip if reading
	}                                                   // header or incr_r itself)
	while (block_code < 0) {
		block_code += 100;                          // add 100 until between 0 and 99
	}
	return block_code;
}

// Accepts a block (2 numbers), returns appropriate character from *char_table
char blockToChar(int char_code)
{
	return char_table[char_code];
}

// Accepts a raw cipher or a .cip file, decrypts it, returns its content as a string
std::string decrypt(std::string source)
{
	std::string msg = "";                               // final message
	bad_password_exception bad_pw_e;
	bad_format_exception bad_format_e;
	old_version_exception old_ver_e;
	bad_cipher_len_exception bad_ciph_e;
	file_read_exception file_read_e;
	in_options in_opt = {};
	char_table = DEFAULT_CHAR_TABLE;
	bool newer_minor_ver_read = false;

	// Set up source (either code or text file)
	std::string cipher;
	if (getBlock(source,0) == 0 && getBlock(source,2) == 88) {
		cipher = source;                         // decipher source directly if valid header read
	} else {
		std::ifstream in_file;                   // decipher contents of file <source>
		in_file.open(source);
		std::getline(in_file, cipher);
		in_file.close();
		if (!in_file.good() && !in_file.eof()) {
			in_file.clear();
			in_file.open(source + ".cip");
			std::getline(in_file, cipher);
			in_file.close();
			if (!in_file.good() && !in_file.eof()) {
				throw file_read_e;
			}
		}
	}

	// Check header
	if (getBlock(cipher,0) != 0 || getBlock(cipher,2) != 88) {
		throw bad_format_e;                      // check for invalid header, throw exception
	} else if ((getBlock(cipher,4) / 10) > (CURRENT_VER_CODE / 10)) {     // check first digit of VER_CODE
		throw old_ver_e;                         // exception if out of date
	} else if ((getBlock(cipher,4) / 10) == (CURRENT_VER_CODE / 10)
		&& (getBlock(cipher,4) % 10) > (CURRENT_VER_CODE % 10)) {     // check second digit of VER_CODE
		newer_minor_ver_read = true;             // set flag (reading still allowed if contents are recognized)
	} else if (cipher.size() % 2 == 1) {
		throw bad_ciph_e;                        // check length - odd numbers would segfault
	} else {
		std::cout << "Read successful.\n\n";
	}

	// Get incr_r
	incr_r = getBlock(cipher, 6);                    // read incr_r from index 6 (right after header)

	// Pre-read post-tilde commands
	bool invalid_ptc_read = false;
	int post_index = cipher.size() - 2;
	int post_buffer = getBlock(cipher, post_index);  // initialize buffer to last block
	while (blockToChar(post_buffer) != '~') {        // so this loop doesn't run at all if there are no PTCs
		switch(blockToChar(post_buffer)) {
			case 'p':
				in_opt.password = true;
				break;
			case 't':
				in_opt.timestamp = true;
				break;
			case 'r':
				in_opt.rand_table = true;
				break;
			default:
				invalid_ptc_read = true;
				break;
		}
		post_index -= 2;
		if (post_index == 4) {
			throw bad_format_e;              // exception if we get to the header; prevents segfault
		}
		post_buffer = getBlock(cipher, post_index);
	}
	if (invalid_ptc_read && newer_minor_ver_read) {
		throw old_ver_e;                         // exception if program has old minor ver and new stuff found
	}

	// Start reading message
	int index = 8;                                   // continue reading from index 8 later
	int buffer = 0;

	// Read password (if applicable)
	if (in_opt.password) {
		std::string password = "";
		std::string pw_input;
		while (blockToChar(getBlock(cipher, index)) != '\n') {    // read password data
			buffer = getBlock(cipher, index);
			password += blockToChar(buffer);
			index += 2;
		}
		index += 2;                              // skip newline after password

		// Check password
		std::cout << "\nThis message is password protected. Enter the password:\n> ";
		std::cin >> pw_input;
		if (pw_input != password) {
			throw bad_pw_e;
		}
		std::cout << std::endl;
	}

	// Read timestamp (if applicable)
	if (in_opt.timestamp) {
		std::string t_str = "";
		while (blockToChar(getBlock(cipher, index)) != '\n') {
			buffer = getBlock(cipher, index);
			t_str += blockToChar(buffer);
			index += 2;
		}
		index += 2;                              // skip newline after timestamp
		std::time_t t = std::stoi(t_str);
		char timestamp[80];
		std::strftime(timestamp, 80, "%A %d %B %Y, %I:%M %p", std::localtime(&t));
		msg += timestamp;
		msg += '\n';
	}

	// Read random table (if applicable)
	if (in_opt.rand_table) {
		char new_table[100];                                      // set up new table
		for (int i = 0; i < 96; i++) {                            // only replace std chars 0-95
			buffer = getBlock(cipher, index);
			new_table[i] = blockToChar(buffer);               // read using old table
			index += 2;
		}
		char_table = new_table;                                   // switch to new table
		index += 2;                                               // skip newline afterwards
	}

	// Read message
	while (getBlock(cipher, index) != 94) {                           // loop through rest (~ is ALWAYS def. table!)
		buffer = getBlock(cipher, index);                         // save block into buffer

		// Handle converting standard chars from table (incl. newline)
		if (buffer >= 0 && buffer < 96) {
			msg += blockToChar(buffer);
		}

		// Handle repeat flag (99)
		if (buffer == 99) {
			index += 2;
			int rep_count = getBlock(cipher, index);          // read number of repetitions
			index += 2;
			int rep_char = getBlock(cipher, index);           // read char
			for (int i = 0; i < rep_count; i++) {
				msg += blockToChar(rep_char);
			}
		}
		index += 2;                                               // move index to next block
	}
	return msg;
}

void readDialog()
{
	clearscr();
	std::string src;
	std::cout << "\n*** READ MODE ***"
	          << "\nEnter the cipher you wish to decrypt or the name of the file you wish to read from."
		  << "\n> ";
	std::cin >> src;
	std::cout << std::endl;
	try {
		std::string result_message = decrypt(src);
		std::cout << "Enter a tilde to display the message here or a filename to save it into a file."
		  	  << "\n> ";
		std::string filename;
		std::cin >> filename;
		if (filename == "~") {
			std::cout << std::endl << result_message;
			copyToClipboard(result_message);
		} else {
			std::ofstream out_msg;
			out_msg.open(filename, std::ios::trunc);          // overwrite existing file
			out_msg << result_message;
			out_msg.close();
			if (out_msg.good()) {
				std::cout << "\nFile " << filename << " written successfully.";
			} else {
				std::cout << "\nError writing file " << filename << ".\n";
			}
		}	
	} catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
	std::cout << "\n\nPress Enter to return." << std::endl;
	std::cin.ignore();                                                // pause
	std::cin.get();
}
