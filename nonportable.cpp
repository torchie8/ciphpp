// This file contains code that is not portable across systems.
#include "global.h"

// clear screen
void clearscr()
{
	system("clear");  // Linux
	//system("cls");    // Windows
}

// copy to clipboard (do nothing; windows only)
void copyToClipboard(std::string str) {}
