#ifndef GLOBAL_H 
#define GLOBAL_H

#include <algorithm>
#include <iomanip>
#include <ctime>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <random>
#include <exception>
#include "version.h"
#include "nonportable.h"

// size of default char table
const int DEFAULT_TABLE_SIZE = 100;

// output options (post-tilde commands)
struct out_options {
	bool timestamp;
	bool rand_table;
	bool password;
};

// input options (post-tilde commands)
struct in_options {
	bool timestamp;
	bool rand_table;
	bool password;
};

#endif
