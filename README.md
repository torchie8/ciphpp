# ciph++

#####For when you're dying to say something, but want nobody to understand.

ciph++ is a lightweight console program written in C++ that can encrypt and decrypt ASCII text using a very simple custom algorithm.

Some features:

* Simple encryption that produces a neat string of numbers
* Easy to use
* Has additional features such as timestamps, passwords...
* Supports Linux and Windows

###Disclaimer:

ciph++ uses a rather primitive proprietary encryption and is NOT safe or intended for actual serious use. If you require reliable, secure encryption, ciph++ is not suitable for your needs. You could say this program is a total gimmick.

###Simple build instructions:

To build for Linux, run `make`. This uses clang++ by default.

To build for Win32, run `make win`. This uses MinGW by default.

If you wish to use different compilers, add `CC=[compiler]` to the make command. Example: `make CC=g++`

To delete the binaries and object files, run `make clean`.
