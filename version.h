#ifndef VERSION_H
#define VERSION_H

#include <iostream>

const int CURRENT_VER_CODE = 10;

const std::string CURRENT_VER = "1.01";

#endif
