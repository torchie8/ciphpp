// This file contains code that is not portable across systems.
#include "global.h"
#include <windows.h>

// clear screen
void clearscr()
{
	//system("clear");  // Linux
	system("cls");    // Windows
}

// copy to clipboard
void copyToClipboard(std::string str)
{
	const char* output = str.c_str();
	const std::size_t len = strlen(output) + 1;
	HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, len);
	memcpy(GlobalLock(hMem), output, len);
	GlobalUnlock(hMem);
	OpenClipboard(0);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
	std::cout << "\n* Copied to clipboard. *\n";
}
