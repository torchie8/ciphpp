// Header file for code that is not portable across systems.
#ifndef NONPORTABLE_H
#define NONPORTABLE_H
#include <string>

// clear screen
void clearscr();

// copy to clipboard (WIN ONLY)
void copyToClipboard(std::string str);

#endif
