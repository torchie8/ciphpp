/* * * * * * * * * * * * * * * *
 * program: ciph++             *
 * version: 1.0_10             *
 * date: 2016/11/22            *
 * author: Matt Black          *
 * * * * * * * * * * * * * * * */

// ciph++ is a simple command line utility that encrypts plain ASCII text into a numeric cipher.
// It serves solely as an amusement tool and for limited personal use. The proprietary encryption
// is NOT to be considered safe or suitable for serious application.
// The author will not be responsible for any damage caused by misuse of the program.

#include "nonportable.h"
#include "version.h"
#include "encrypt.h"
#include "decrypt.h"
#include "help.h"
#include <iostream>

// display main menu in a loop
void mainMenu()
{
	while (true) {
		clearscr();
		
		// print greeting
		std::cout << "\n\t* * * * * * * * * * * * * * *"
			  << "\n\tciph++ v" << CURRENT_VER << "_" << CURRENT_VER_CODE << " by Matt Black"
			  << "\n\t* * * * * * * * * * * * * * *"
			  << "\n\nPress one of the following keys:"
			  << "\n - [W]rite a cipher"
			  << "\n - [R]ead a cipher"
			  << "\n - [H]elp"
			  << "\n - [Q]uit\n\n> "; 
		char sel;
		std::cin.get(sel);
		switch(sel) {
			case 'w': case 'W':
				writeDialog();
				break;
			case 'r': case 'R':
				readDialog();
				break;
			case 'h': case 'H':
				helpScreen();
				break;
		}
		if (sel == 'q' || sel == 'Q') {
			break;
		}
	}
}

int main()
{
	mainMenu();
	clearscr();
	return 0;
}
