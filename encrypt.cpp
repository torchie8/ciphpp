// This part of the program accepts input and encrypts it.

#include "global.h"

class invalid_char_exception: public std::exception
{
	virtual const char *what() const throw()
	{
		return "Aborting: invalid character found in input";
	}
};

// TODO: implement copying to clipboard

// incrementor
int incr_w;

// create random number generator
std::random_device rd;
std::mt19937 mt(rd());
std::uniform_int_distribution<int> rand_num(0,99);

// table of standard characters which may be used in a message
char DEFAULT_CHAR_TABLE [DEFAULT_TABLE_SIZE] = {
	' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
	'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
	'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c',
	'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
	'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6',
	'7', '8', '9','\'', '"', ':', ';', '.', ',', '?',
	'-', '_', '/', '|','\\', '[', ']', '{', '}', '(',
	')', '+', '=', '!', '@', '#', '$', '%', '^', '&',
	'*', '<', '>','\n', '~','\t'
};

// for swapping tables
char *char_table = DEFAULT_CHAR_TABLE;

// Accepts an int value, returns it as string prepended by 0 if < 9 and truncated if > 99
std::string toBlock(int num)
{
	num = num % 100;
	std::string res_block = "";
	if (num < 10) {
		res_block += "0";                    // prepend with "0" if < 10 (e.g. "08")
	}
	res_block += std::to_string(num);
	return res_block;
}

// Accepts a single char and returns its table code as string incremented by incr_w 
std::string encrypt(char msg_char)
{
	invalid_char_exception inv_char_e;
	int char_code = 0; // tracks position in table, then used for char -> code conversion
	while (true) {
		if (msg_char == char_table[char_code]) {
			return toBlock(char_code + incr_w);
		} else {
			char_code++;
			if (char_code > 99) {
				throw inv_char_e;    // throw ex if char not in table
			}
		}
	}
}

// Loop through the message in parameter, convert the string into a cipher and return the result
std::string makeCipher(std::string msg_line)
{	
	std::string ciph = "";
	for (std::string::size_type i = 0; i < msg_line.size(); i++) {
		ciph += encrypt(msg_line[i]);
	}
	return ciph;
}

// The user interface for inputting and encrypting a message
void writeDialog()
{	
	// Randomize the cipher (combines true and pseudo-randomness)
	incr_w = rand_num(mt) % 100;

	// Read input from the user and store it
	std::vector<std::string> msg;               // contains raw message separated into lines
	std::string line;                           // contains most recently input raw line
	std::string result;                         // contains the finished encrypted code
	int eof_pos;                                // contains position of EOF (tilde)
	out_options out_opt = {};
	std::string post_tilde_cmd = "";

	// Intro message
	clearscr();
	std::cout << "\n*** WRITE MODE ***"
		  << "\nEnter the message you wish to encrypt. May only contain ASCII characters."
		  << " Newline is supported.\nMessage must end with a tilde: ~"
		  << "\nSpecial commands can be added past the tilde:"
		  << "\n\tp - message will be password protected"
		  << "\n\tt - timestamp will be included with the message"
		  << "\n\tr - randomize encoding table (warning: long result)\n";

	// Input loop - read input until tilde is found
	std::cin.ignore();                          // ignore trailing newline
	while (true) {
		bool end_input = false;
		std::cout << "> ";
		std::getline(std::cin, line);       // read 1 line
		for (std::string::size_type i = 0; i < line.size(); i++) {
			if (line[i] == '~') {
				end_input = true;   // end input when tilde found
				eof_pos = i;        // save EOF pos
				break;
			}
		}
		if (!end_input) {
			line += '\n';               // append a newline symbol
		}
		msg.push_back(line);                // add to raw message vector
		if (end_input) {
			break;
		}
	}

	// Read post-tilde commands, print confirmations
	for (std::string::size_type i = eof_pos + 1; i < line.size(); i++) {
		switch(line[i]) {
			case 't':
				out_opt.timestamp = true;          // include timestamp with message
				post_tilde_cmd += "t";
				std::cout << "\nt - timestamp will be included.";
				break;
			case 'r':
				out_opt.rand_table = true;         // randomize char table
				post_tilde_cmd += "r";
				std::cout << "\nr - random table will be included.";
				break;
			case 'p':
				out_opt.password = true;           // enable password protection
				post_tilde_cmd += "p";
				std::cout << "\np - message will be password protected.";
				break;
		}
	}

	// Encrypt the stored input and output it as a file or a string
	// Get password from user (if enabled)
	std::string password;
	if (out_opt.password) {
		std::cout << "\nEnter the desired password. It will be required to decrypt the message.\n> ";
		std::cin >> password;
	}
		
	// Produce output
	try {
		// Write header and incr_w
		result = "0088";                                       // header base
		result += toBlock(CURRENT_VER_CODE) + toBlock(incr_w); // add version code and incr_w

		// Encrypt password (if applicable)
		if (out_opt.password) {
			result += makeCipher(password) + encrypt('\n');;
		}

		// Encrypt timestamp (if applicable)
		if (out_opt.timestamp) {
			std::time_t t = std::time(nullptr);
			result += makeCipher(std::to_string(t));
			result += encrypt('\n');
		}

		// Encrypt random table (if applicable)
		if (out_opt.rand_table) {
			char rand_table[100];
			std::copy(&DEFAULT_CHAR_TABLE[0], &DEFAULT_CHAR_TABLE[100], &rand_table[0]);
			std::shuffle(&rand_table[0], &rand_table[95], mt);  // make new table and shuffle it using mt19937
			for (int i = 0; i < 96; i++) {
				result += encrypt(rand_table[i]);      // write new table using old table
			}                                              // char codes in old order
			result += encrypt('\n');
			char_table = rand_table;                       // switch to new table
		}

		// Encrypt message
		for (std::string::size_type i = 0; i < msg.size(); i++) {
			for (std::string::size_type j = 0; j < msg[i].size(); j++) {

				// Break if tilde found (don't include it yet)
				if (msg[i][j] == '~') {
					break;
				}

				// Check & replace 3+ duplicate characters (use 99 flag)
				if (msg[i][j] == msg[i][j+1] && msg[i][j] == msg[i][j+2]) {   // always check 2 chars fwd.
					int rep_count = 0;        // tracks number of duplicates (min. 3)
					int rep_index = j;        // move independently of index j
					while (msg[i][rep_index] == msg[i][j]) {
						rep_count++;
						rep_index++;
					}

					// Encrypt this in the following format: ...|99|rep_count|char|...
					result += toBlock(99 + incr_w) + toBlock(rep_count + incr_w) + encrypt(msg[i][j]);
					j = rep_index;            // move j -> continue reading past the duplicates
				}

				// Write normal characters
				result += encrypt(msg[i][j]);
			}
		}

		// Revert to default table (so that PTCs can always be read)
		char_table = DEFAULT_CHAR_TABLE;
		result += encrypt('~');
		result += makeCipher(post_tilde_cmd);

		// Save output
		std::cout << "\nSpecify filename to save the output into. Existing files will be "
		          << "overwritten.\nEnter a tilde (~) to display it here instead.\n> ";
		std::string filename;
		std::cin >> filename;                                        // get filename (or ~)
		bool has_ext = false;
		if (filename == "~") {
			std::cout << "\nResult: " << result << std::endl;    // output into console
			copyToClipboard(result);
		} else {
			std::ofstream out_file;
			if (filename.size() > 4                              // check if filename already has extension
			 && filename[filename.size()-1] == 'p' && filename[filename.size()-2] == 'i'
			 && filename[filename.size()-3] == 'c' && filename[filename.size()-4] == '.') {
				out_file.open(filename, std::ios::trunc);    // save it as is if so
				has_ext = true;
			} else {
				out_file.open((filename + ".cip"), std::ios::trunc); // else add .cip extension
			}
			out_file << result;
			out_file.close();
			if (out_file.good()) {                               // report success
				std::cout << "\nFile " << filename;
				if (!has_ext) {
					std::cout << ".cip";
				}
				std::cout << " written successfully.\n";
			} else {                                             // report failure
				std::cout << "\nError writing file " << filename << ".cip.\n";
			}
		}
		std::cout << "\n\nPress Enter to return." << std::endl;
	} catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
	}
	std::cin.ignore();                                                   // pause
	std::cin.get();
}
