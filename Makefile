# Makefile for ciph++
# created by Matt Black 2016/11/26

PROG = ciphpp
win : PROG = ciphpp.exe
CC = clang++
win : CC = i686-w64-mingw32-g++
NONPORT = nonportable.cpp
win : NONPORT = nonportable_win.cpp
CFLAGS = -std=c++11 -Wall -pedantic
win : CFLAGS = -std=c++11 -Wall -pedantic -static-libgcc -static-libstdc++
OBJS = main.o decrypt.o encrypt.o help.o nonportable.o

# Compiling for Linux
linux : $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS)

# (Cross)compiling for Windows
win : $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS)

main.o : version.h decrypt.h encrypt.h help.h nonportable.h main.cpp
	$(CC) $(CFLAGS) -c main.cpp

encrypt.o : global.h encrypt.cpp
	$(CC) $(CFLAGS) -c encrypt.cpp

decrypt.o : global.h decrypt.cpp
	$(CC) $(CFLAGS) -c decrypt.cpp

help.o : global.h version.h help.cpp
	$(CC) $(CFLAGS) -c help.cpp

nonportable.o : #(NONPORT)
	$(CC) $(CFLAGS) -c $(NONPORT) -o nonportable.o

clean :
	rm -f $(OBJS) ciphpp ciphpp.exe
