#include "global.h"
#include "version.h"

void showWriteHelp()
{
	// Screen 1
	clearscr();
	std::cout << "\n\t\tciph++ v" << CURRENT_VER << " (code ver " << CURRENT_VER_CODE << ") by Matt Black\n\n";
	std::cout << "\t** WRITE MODE GUIDE **"
		  << "\n\nWrite Mode is where you create your ciphers. ciph++ can encrypt any"
		  << "\nnon-special base ASCII charactes including whitespace. This makes it"
		  << "\npossible to encrypt source code, for example. A limitation is that"
		  << "\nthe tilde (~) cannot be used, as it halts input when detected. The"
		  << "\nprogram will thus read your input until you write a tilde at the end."
		  << "\nAnything past the tilde is considered a post-tilde command."
		  << "\n\n * Post-Tilde Commands *"
		  << "\nPost-tilde commands (PTCs) are parameters included after the tilde which"
		  << "\nends your message. They have various effects, all of which are explained"
		  << "\nwhen you enter Write Mode."
		  << "\n\nExample: > test~pt   // writes message \"test\" with a timestamp and"
		  << "\n                     // a password which you will then be asked to set"
		  << "\n\n\n\tPress Enter to continue."
		  << std::endl;
	std::cin.ignore();
	std::cin.get();

	// Screen 2
	clearscr();
	std::cout << "\n\t\tciph++ v" << CURRENT_VER << " (code ver " << CURRENT_VER_CODE << ") by Matt Black\n\n";
	std::cout << "\t** WRITE MODE GUIDE **"
		  << "\n\n * Saving *"
		  << "\nFinished ciphers can be output directly or saved into files with the"
		  << "\n.cip extension. After you're done entering your message, you will be"
		  << "\nasked to input a filename."
		  << "\n\nExample: > MyCipher   // saves into a file named MyCipher.cip"
		  << "\n\nEntering a single tilde (~) displays the cipher directly on your"
		  << "\nscreen instead of saving a file."
		  << "\n\nExample: > ~"
		  << "\n         Result: 0088xx...   // outputs raw cipher"
		  << "\n\nThis result is automatically copied to clipboard for Windows users."
		  << "\nLinux users must select and copy it manually. After you have copied"
		  << "\nyour cipher, press Enter to return to the main menu."
		  << "\n\n\n\tPress Enter to return."
		  << std::endl;
	std::cin.get();
}

void showReadHelp()
{
	clearscr();
	std::cout << "\n\t\tciph++ v" << CURRENT_VER << " (code ver " << CURRENT_VER_CODE << ") by Matt Black\n\n";
	std::cout << "\t** READ MODE GUIDE **"
		  << "\n\nRead Mode is where you decrypt existing ciph++ ciphers. Upon entering"
		  << "\nRead Mode, you will be asked to input a source. This can be either"
		  << "\na raw cipher in the 0088xx... format, or a .cip file containing one."
		  << "\n\nExample: > MyCipher.cip   // reads from the file called MyCipher.cip"
		  << "\nExample: > 008810206651656614   // reads this cipher directly"
		  << "\n\nIf the cipher is invalid or the file does not exist, an error message"
		  << "\nwill be shown."
		  << "\n\nIf the message is password protected, you will be asked to enter"
		  << "\nthe password. The message will not open if the password is incorrect."
		  << "\n\nYou will be asked to enter a filename to save the message into. If"
		  << "\nyou want the message displayed directly, enter a tilde instead:"
		  << "\n\nExample: > ~"
		  << "\n\nThis result is automatically copied to clipboard for Windows users."
		  << "\n\nWhen you're done, press Enter to return to the main menu."
		  << "\n\n\n\tPress Enter to return."
		  << std::endl;
	std::cin.ignore();
	std::cin.get();
}

void helpScreen()
{
	while (true) {
		// Print intro help text
		clearscr();
		std::cout << "\n\t\tciph++ v" << CURRENT_VER << " (code ver " << CURRENT_VER_CODE << ") by Matt Black\n\n";
		std::cout << "Welcome to ciph++, a simple and quick text ciphering program."
		  	<< "\nTo use the program, just follow the onscreen instructions."
		  	<< "\n\nWhat ciph++ can do:"
		  	<< "\n - encrypt plain ASCII text such as personal notes or code"
		  	<< "\n - write messages readable only by other ciph++ users"
		  	<< "\n - protect your text with a password"
		  	<< "\nWhat ciph++ can NOT do:"
		  	<< "\n - provide reliable, secure encryption"
		  	<< "\n - encrypt non-base-ASCII text or any other type of data"
		  	<< "\n\nciph++ is not a perfect program, and is not intended for use in"
		  	<< "\nserious, important applications. The author will not be held"
		  	<< "\nresponsible for any damage caused by misuse of this program."
		  	<< "\n\nPress one of the following keys followed by Enter:"
		  	<< "\n - [W]rite mode help section"
		  	<< "\n - [R]ead mode help section"
		  	<< "\n - [Q]uit to main menu"
		  	<< std::endl;

		// Wait for selection
		char help_sel;
		std::cin.get(help_sel);
		switch(help_sel) {
			case 'w': case 'W':
				showWriteHelp();              // print write mode help section
				break;
			case 'r': case 'R':
				showReadHelp();               // print read mode help section
				break;
		}
		if (help_sel == 'q' || help_sel == 'Q') {
			break;                                // quit to main menu
		}
	}
}
